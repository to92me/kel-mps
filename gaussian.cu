
#include "utils.h"

__global__ void gaussian_blur(const unsigned char* const input_channel,
	unsigned char* const output_channel,
	int rows, int cols,
	const float* const filter, const int filter_width)
{

	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x >= cols || y >= rows){
		return;
	}

	float calc = 0;

	//x 
	for (int i = -filter_width / 2; i <= filter_width / 2; i++)
	{
		int temp_x = x + i;
		if (temp_x < 0){
			temp_x = 0;
		}else if (temp_x >= cols){
			temp_x = cols - 1;
		}

		//y 
		for (int j = -filter_width / 2; j <= filter_width / 2; j++){
			int temp_y = y + j;

			if (temp_y < 0){
				temp_y = 0;
			}else if (temp_y >= rows){
				temp_y = rows - 1;
			}

			float filter_val = filter[filter_width / 2 + i + (filter_width / 2 + j) * filter_width];
			calc += filter_val * (float)input_channel[temp_x + temp_y * cols];
		}
	}
	output_channel[x + y * cols] = (char)calc;
}

__global__ void separateChannels(const uchar4* const input_image,
	int rows,
	int cols,
	unsigned char* const red_channel,
	unsigned char* const green_channel,
	unsigned char* const blue_channel)
{

	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x >= cols || y >= rows){
		return;
	}

	int pixel = x + y * cols;

	red_channel[pixel] = input_image[pixel].x;
	green_channel[pixel] = input_image[pixel].y;
	blue_channel[pixel] = input_image[pixel].z;
}

__global__
void recombineChannels(const unsigned char* const red_channel,
	const unsigned char* const green_channel,
	const unsigned char* const blue_channel,
	uchar4* const output_image,
	int rows,
	int cols)
{
	const int2 thread_2D_pos = make_int2(blockIdx.x * blockDim.x + threadIdx.x,
		blockIdx.y * blockDim.y + threadIdx.y);

	const int thread_1D_pos = thread_2D_pos.y * cols + thread_2D_pos.x;

	if (thread_2D_pos.x >= cols || thread_2D_pos.y >= rows)
		return;

	unsigned char red = red_channel[thread_1D_pos];
	unsigned char green = green_channel[thread_1D_pos];
	unsigned char blue = blue_channel[thread_1D_pos];

	uchar4 outputPixel = make_uchar4(red, green, blue, 255);
	output_image[thread_1D_pos] = outputPixel;
}

unsigned char *d_red, *d_green, *d_blue;
float         *d_filter;

void allocateMemoryAndCopyToGPU(const size_t rowsImage, const size_t colsImage,
	const float* const h_filter, const size_t filter_width)
{

	checkCudaErrors(cudaMalloc(&d_red, sizeof(unsigned char) * rowsImage * colsImage));
	checkCudaErrors(cudaMalloc(&d_green, sizeof(unsigned char) * rowsImage * colsImage));
	checkCudaErrors(cudaMalloc(&d_blue, sizeof(unsigned char) * rowsImage * colsImage));
	checkCudaErrors(cudaMalloc(&d_filter, sizeof(float) * filter_width * filter_width)); 
	checkCudaErrors(cudaMemcpy(d_filter, h_filter, sizeof(float) * filter_width * filter_width, cudaMemcpyHostToDevice));
}

void execute_gaussian_blur(const uchar4 * const h_input_image, uchar4 * const d_input_image,
	uchar4* const d_output_image, const size_t rows, const size_t cols,
	unsigned char *d_redBlurred,
	unsigned char *d_greenBlurred,
	unsigned char *d_blueBlurred,
	const int filter_width)
{
	const dim3 blockSize(32, 32, 1);
	const dim3 gridSize((cols - 1) / blockSize.x + 1, (rows - 1) / blockSize.y + 1, 1);
	separateChannels<<<gridSize, blockSize>>>(d_input_image, rows, cols, d_red, d_green, d_blue);

	
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	gaussian_blur<<<gridSize, blockSize>>>(d_red, d_redBlurred, rows, cols, d_filter, filter_width);
	gaussian_blur<<<gridSize, blockSize>>>(d_green, d_greenBlurred, rows, cols, d_filter, filter_width);
	gaussian_blur<<<gridSize, blockSize>>>(d_blue, d_blueBlurred, rows, cols, d_filter, filter_width);

	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	recombineChannels<<<gridSize, blockSize>>>(d_redBlurred,
		d_greenBlurred,
		d_blueBlurred,
		d_output_image,
		rows,
		cols);

	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}


void cleanup() {
  checkCudaErrors(cudaFree(d_red));
  checkCudaErrors(cudaFree(d_green));
  checkCudaErrors(cudaFree(d_blue));
  checkCudaErrors(cudaFree(d_filter));
}