FROM nvidia/cuda:11.2.0-devel-ubuntu18.04


ENV DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt install -y tzdata
RUN echo "Europe/Belgrade" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata
ENV TZ=Europe/Belgrade
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt install libopencv-dev -y

COPY compare.cpp /opt/gaussian/compare.cpp
COPY compare.h /opt/gaussian/compare.h
COPY gaussian.cu /opt/gaussian/gaussian.cu
COPY pre_post_process.cpp /opt/gaussian/pre_post_process.cpp
COPY main.cpp /opt/gaussian/main.cpp
COPY Makefile /opt/gaussian/Makefile
COPY memristor.jpeg /opt/gaussian/memristor.jpeg
COPY reference_calc.cpp /opt/gaussian/reference_calc.cpp
COPY reference_calc.h /opt/gaussian/reference_calc.h
COPY timer.h /opt/gaussian/timer.h
COPY utils.h /opt/gaussian/utils.h 

WORKDIR /opt/gaussian/

RUN make
