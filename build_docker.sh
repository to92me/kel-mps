#!/bin/bash          
CONTAINER_TAG=mps:1.0
CONTAINER_NAME=mps-gaussian

printf 'Building ${CONTAINER_TAG} \n'

docker build -f  gaussian.Dockerfile -t $CONTAINER_TAG .

printf '\nStoppping old container \n'
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME

printf '\nRunning a new one \n'

docker run -it -d --gpus=all \
    --restart unless-stopped \
    -v /media/:/media \
    --name=$CONTAINER_NAME \
    $CONTAINER_TAG